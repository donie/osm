<h1>OSM signpost gather</h1>

This project uses cocoapods and mogenerator as part of the build

<h2>What it does</h2>

This app creates notes when you press the relevant signpost on the screen and logs it to OSM as a note which can be edited later.

The app will work offline as it stores all tracked locations in core data and sends when it has a connection to 3G/4G/Wifi

