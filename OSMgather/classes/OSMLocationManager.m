//
//  OSMLocationManager.m
//
//  Created by Donie Kelly on 27/02/2014.
//  Copyright (c) 2014 Doppeltime. All rights reserved.
//

#import "OSMLocationManager.h"

@interface OSMLocationManager ()

@property(nonatomic, strong) OSMLocation *currentLocation;
@property(nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation OSMLocationManager

static OSMLocationManager *sharedInstance;

+(instancetype)shared
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        sharedInstance.locationManager = [CLLocationManager new];
        sharedInstance.locationManager.delegate = sharedInstance;
        sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        [sharedInstance.locationManager startUpdatingLocation];
    });
    return sharedInstance;
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *current = [locations lastObject];
    if(current)
    {
        if(-[current.timestamp timeIntervalSinceNow] > 20)
            return;
        
        if (current.horizontalAccuracy < 0)
            return;
        
        sharedInstance.currentLocation = (OSMLocation *)current;
    }
}

-(void) startUpdating
{
    [sharedInstance.locationManager startUpdatingLocation];
}

-(void) stopUpdating
{
    [sharedInstance.locationManager stopUpdatingLocation];
}

-(OSMLocation *) getUserLocation
{
    return sharedInstance.currentLocation;
}

@end
