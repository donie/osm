//
//  RootViewController.m
//  OSMgather
//
//  Created by Donie Kelly on 02/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import "RootViewController.h"
#import "LoggingViewController.h"

@interface RootViewController ()
{
    User *user;
}
@property(nonatomic, strong) OsmService *service;
@end


@implementation RootViewController

-(void) viewDidLoad
{
    [super viewDidLoad];
    self.service = [OsmService new];
}

-(IBAction) register:(id)sender
{
    NSURL *registerURL = [NSURL URLWithString:@"http://api06.dev.openstreetmap.org/user/new"];
    [[UIApplication sharedApplication] openURL:registerURL];
}

- (IBAction)login:(id)sender
{
    user = [User getUserWithUsername:self.usernameField.text];
    if(!user)
        user = [User createUserWith:self.usernameField.text andPassword:self.passwordField.text];
    
    [self.service getUserDetails:user successBlock:^(id results)
    {
        user.delegate = self; // Calls back to xmlParser complete below
        
        NSXMLParser *XMLParser = (NSXMLParser*)results;
        XMLParser.delegate = user;
        [XMLParser parse];
    }
    errorBlock:^(NSError *error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:@"Invalid username or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}

-(void) xmlParseComplete
{
    // Log out all other users
    [User logoutAll];
    [user setActiveValue:YES];
    [user save];
    [self performSegueWithIdentifier:@"start" sender:nil];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    LoggingViewController *vc = segue.destinationViewController;
    vc.service = self.service;
}

@end
