//
//  NSData+AES256.h
//  OSMgather
//
//  Created by Donie Kelly on 03/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import <CommonCrypto/CommonCryptor.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
