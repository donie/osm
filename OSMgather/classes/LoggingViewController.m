//
//  LoggingViewController.m
//  OSMgather
//
//  Created by Donie Kelly on 03/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import "LoggingViewController.h"

@implementation LoggingViewController

-(void) viewDidLoad
{
    self.mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [[OSMLocationManager shared] startUpdating];
    [super viewDidAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [[OSMLocationManager shared] stopUpdating];
    [super viewWillDisappear:animated];
}

- (IBAction)speedCameraSign:(id)sender
{
    [self createEntity:@"Speed camera signpost"];
}
- (IBAction)thirty:(id)sender {
    [self createEntity:@"30kph signpost"];
}
- (IBAction)fifty:(id)sender {
    [self createEntity:@"50kph signpost"];
}
- (IBAction)sixty:(id)sender {
    [self createEntity:@"60kph signpost"];
}
- (IBAction)eighty:(id)sender {
    [self createEntity:@"80kph signpost"];
}
- (IBAction)onehundred:(id)sender {
    [self createEntity:@"100kph signpost"];
}
- (IBAction)onetwenty:(id)sender {
    [self createEntity:@"120kph signpost"];
}

-(void) createEntity:(NSString *)note
{
    
    OSMLocation *location = [[OSMLocationManager shared] getUserLocation];
    Entity *osmNote = [Entity MR_createEntity];
    osmNote.latitudeValue = location.coordinate.latitude;
    osmNote.longitudeValue = location.coordinate.longitude;
    osmNote.note = note;
    [osmNote save];
    [self postNote];
}

-(void) postNote
{
    NSArray *notes = [Entity MR_findByAttribute:EntityAttributes.sent withValue:[NSNumber numberWithBool:NO]];
    for(Entity *note in notes)
    {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:note.latitudeValue longitude:note.longitudeValue];
        [self.service postNote:note.note forUser:[User getActive] coordinate:location successBlock:^(id response)
         {
             note.sentValue = YES;
             [note save];
         }
        errorBlock:^(NSError *error)
         {
             NSLog(@"%@", error);
         }];
    }
}

@end
