//
//  OsmService.m
//  OSMgather
//
//  Created by Donie Kelly on 02/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//


#import "OsmService.h"

@implementation OsmService

-(NSString *) urlForService:(NSString *) path
{
    return [NSString stringWithFormat:@"http://api06.dev.openstreetmap.org%@", path];
}

- (void)getUserDetails:(User *)user
    successBlock:(void (^)(id))successBlock errorBlock:(void (^)(NSError *))errorBlock
{
    NSString *serviceURL = [self urlForService:@"/api/0.6/user/details"];
        
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:user.username password:[user getDecryptedPassword]];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    manager.responseSerializer = [AFXMLParserResponseSerializer new];
    [manager GET:serviceURL parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        successBlock(responseObject);
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        errorBlock(error);
    }];
}

- (void)postNote:(NSString *)note forUser:(User *)user
      coordinate:(CLLocation *)location
           successBlock:(void (^)(id))successBlock errorBlock:(void (^)(NSError *))errorBlock
{
    NSString *serviceURL = [self urlForService:@"/api/0.6/notes"];
    NSString *modNote = [NSString stringWithFormat:@"%@ - this note was added automatically by the OSM Signpost Mapper app - see https://itunes.apple.com/us/app/osm-signpost-mapper/id854694338?ls=1&mt=8", note];
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:@"lat"];
    [dict setObject:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"lon"];
    [dict setObject:modNote forKey:@"text"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFXMLParserResponseSerializer new];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:user.username password:[user getDecryptedPassword]];
    [manager POST:serviceURL parameters:dict
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         successBlock(responseObject);
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         errorBlock(error);
     }];
}

@end
