//
//  OsmService.h
//  OSMgather
//
//  Created by Donie Kelly on 02/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface OsmService : NSObject

- (void)getUserDetails:(User *)user
          successBlock:(void (^)(id))successBlock errorBlock:(void (^)(NSError *))errorBlock;

- (void)postNote:(NSString *)note forUser:(User *)user
      coordinate:(CLLocation *)location
    successBlock:(void (^)(id))successBlock errorBlock:(void (^)(NSError *))errorBlock;

@end
