//
//  BorderedView.m
//  OSMgather
//
//  Created by Donie Kelly on 03/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import "BorderedView.h"

@implementation BorderedView

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    self.layer.borderColor = [[UIColor redColor] CGColor];
    self.layer.borderWidth = 2.0;
    self.layer.cornerRadius = 10.0;
    self.clipsToBounds = YES;
    self.layer.opaque = YES;
}


@end
