//
//  LoggingViewController.h
//  OSMgather
//
//  Created by Donie Kelly on 03/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import "OsmService.h"
#import <MapKit/MapKit.h>

@interface LoggingViewController : UIViewController

@property(nonatomic, strong) IBOutlet MKMapView *mapView;
@property(nonatomic, strong) OsmService *service;
@end
