//
//  OSMLocationManager.h
//
//  Created by Donie Kelly on 27/02/2014.
//  Copyright (c) 2014 Doppeltime. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface OSMLocation : CLLocation
@end


@interface OSMLocationManager : NSObject <CLLocationManagerDelegate>

+(instancetype)shared;

-(void) startUpdating;
-(void) stopUpdating;

-(OSMLocation *) getUserLocation;

@end
