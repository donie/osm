//
//  RootViewController.h
//  OSMgather
//
//  Created by Donie Kelly on 02/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//


#import "OsmService.h"

@interface RootViewController : UIViewController <UserDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *login;

@end
