#import "_User.h"

@protocol UserDelegate <NSObject>

-(void) xmlParseComplete;

@end

@interface User : _User <NSXMLParserDelegate>

@property(nonatomic, strong) id<UserDelegate> delegate;

+(User *) getActive;
+(void) logoutAll;
+(User *) getUserWithUsername:(NSString *)username;
+(User *) createUserWith:(NSString *)username andPassword:(NSString *)password;

-(void)save;
-(NSData *) setEncryptedPassword:(NSString *)password;
-(NSString *) getDecryptedPassword;
@end
