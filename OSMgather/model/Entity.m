#import "Entity.h"


@interface Entity ()

// Private interface goes here.

@end


@implementation Entity

-(void)save
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
        // TODO - handle errors
    }];
}

@end
