#import "User.h"


@interface User ()
@property(nonatomic, strong) NSString *tmpParse;
@end


@implementation User

@synthesize tmpParse;
@synthesize delegate;

+(void) logoutAll
{
    NSArray *users = [User MR_findByAttribute:@"active" withValue:[NSNumber numberWithBool:YES]];
    for(User *user in users)
    {
        [user setActiveValue:NO];
        [user save];
    }
}

+(User *) getActive
{
    return [User MR_findFirstByAttribute:@"active" withValue:[NSNumber numberWithBool:YES]];
}

+(User *) getUserWithUsername:(NSString *)username
{
    return [User MR_findFirstByAttribute:@"username" withValue:username];
}

+(User *) createUserWith:(NSString *)username andPassword:(NSString *)password
{
    User *user = [User MR_createEntity];
    user.username = username;
    user.password = [user setEncryptedPassword:password];
    return user;
}


-(NSData *) setEncryptedPassword:(NSString *)password
{
    NSData* passwd = [password dataUsingEncoding:NSUTF8StringEncoding];
    return [passwd AES256EncryptWithKey:@"^@%@£$%@$%@^£$@$£@%$"];
}

-(NSString *) getDecryptedPassword
{
    NSData *passwd = self.password;
    return [[NSString alloc] initWithData:[passwd AES256DecryptWithKey:@"^@%@£$%@$%@^£$@$£@%$"] encoding:NSUTF8StringEncoding];
}

#pragma mark - Parsing lifecycle
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"user"])
    {
        self.name = [attributeDict valueForKey:@"display_name"];
        self.userId = [attributeDict valueForKey:@"id"];
    }
    
    if ([elementName isEqualToString:@"img"])
        self.imageUrl = [attributeDict valueForKey:@"href"];
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmpParse = string;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    [self.delegate xmlParseComplete];
}

-(void)save
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
        // TODO - handle errors
    }];
}


@end
