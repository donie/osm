//
//  AppDelegate.h
//  OSMgather
//
//  Created by Donie Kelly on 02/04/2014.
//  Copyright (c) 2014 Donie Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
